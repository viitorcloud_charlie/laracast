<?php

//SHIPPING INTERFACE
interface Shipping {
        public function isShippingCharge();
        public function isTrackingAvailable();
        }

//INCLUDE TAX INTERFACE
interface includeTax{
        public function includeTax();
        public function excludeTax();
}

//TRAIT EXAMPLE
trait Properties{
    //TO THROW COMMON EXCEPTION IF FILE DOES NOT EXIST
	    public function getfile($filename)
	    {
	        if (!file_exists($filename)) {
	            throw new Exception('The file ' . $filename . ' does not exist');
	        }
	    }
        public function setTitle($title)
        {

            if(!isset($this->$title)){

                    echo 'Welcome to the '.$title.'..!<br><br>';

            }else{
                throw new Exception('Please Enter any name');
            }
        }

}

//ORDER CLASS THAT IMPLEMENTS SHIPPING INTERFACE AND USE PROPERTIES TRAIT
class Order implements Shipping,includeTax {
        private $name;
        public function __construct($name,$age) {
                $this->name = $name;
                $this->age  = $age;
                }
        public function getname() {
                return $this->name;
                }
        public function getage() {
                return $this->age;
        }
        //INTERFACE METHODS WHICH NEEDS TO BE IMPLEMENTED
        public function isShippingCharge() {
                echo "Shipping is chargeable.<br>";
        }
         public function isTrackingAvailable() {
                echo "Tracking is Available<br>";
        }
         public function includeTax()
         {
                echo "text included<br>";
         }
        public function excludeTax()
        {
                echo "text excluded<br>";
        }
        //CALL A TRAIT
        use Properties;
}

//PRODUCT CLASS THAT IMPLEMENTS SHIPPING INTERFACE
class Product implements Shipping {
        private $name;

        public function __construct($name,$price) {

                $this->name = $name;
                $this->price = $price;
        }
        public function getname() {
                return $this->name;
                }
         public function getprice() {
                return $this->price;
                }

        //FUNCTION CREATED TO UNDERSTAND THE EXAMPLE OF CUSTOM EXCEPTION
        public function inverse($no) {
            if (!$no) {
                throw new Exception('Division by zero.<br>');
            }
            return 1/$no;

        }

        //INTERFACE METHODS WHICH NEEDS TO BE IMPLEMENTED
         public function isShippingCharge() {
                echo "Shipping is chargeable.<br>";
        }
         public function isTrackingAvailable() {
                echo "Tracking is Available<br>";
        }
}
?>
<html>
<body>

<?php

$charlie  = new Order("Charlie",25);
$Laptop   = new Product("Laptop",0);

//CALL TO TRAIT METHOD
echo $charlie->setTitle("<b>Php Essentials Demo</b>");

//CALL ORDER CLASS METHODS
$custage  = $charlie -> getage();
$custname = $charlie -> getname();

//CALL PRODUCT CLASS METHODS
$proname  = $Laptop  -> getname();
$proprice = $Laptop  -> getprice();
            

print "$custname is $custage years old!<br><br>";
print "$proname is for $proprice ruppes only!<br><br>";

//CALLED INTERFACES METHODS
$charlie -> isShippingCharge();
$charlie -> includeTax();
$Laptop  -> isTrackingAvailable();

//CALL EXCEPTION FUNCTION
try {
    echo $Laptop  ->inverse(0)."<br/>";
}
catch (Exception $e) {
    echo '<br>Caught exception: ',  $e->getMessage(), "<br/>";
}

//CALL TRAIT BLOCK METHOD
echo $charlie->getfile('hello.php').'<br/>';

?>
<hr>
</body>
</html>