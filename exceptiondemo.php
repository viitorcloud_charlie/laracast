
<?php
function inverse($no) {
    if (!$no) {
        throw new Exception('Division by zero.');
    }
    return 1/$no;
}

try {
    echo inverse(5) . "<br/>";
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "<br/>";
}

try {
    echo inverse(0) . "<br/>";
} catch (Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "<br/>";
}

// Continue enoecution
echo "Hello World\n";
?>
